from flask import Flask, session, redirect, url_for, escape, request

app = Flask(__name__)
app.secret_key = "teste"

@app.route('/')
def index():
   if 'username' in session:
      username = session['username']
      return 'Logged in as ' + username + '<br>' + "<b><a href = '/logout'>click here to log out</a></b>"
   else:
      return "You are not logged in <br><a href = '/login'>" + "click here to log in</a>"


@app.route('/login', methods=['GET', 'POST'])
def login():
   if request.method == 'POST':
      session['username'] = request.form['username']
      return redirect(url_for('index'))
   else:
      return "<form action=\"\" method=\"post\">"\
         "<p><input type=\"text\" name=\"username\"/></p>"\
         "<p<<input type=\"submit\" value=\"Login\"/></p>"\
         "</form>"


@app.route('/logout')
def logout():
   # remove the username from the session if it is there
   session.pop('username', None)
   return redirect(url_for('index'))


def iniciar():
   app.run(debug=True, port=3003, host='0.0.0.0')

